Resources
=========

Here's the resources that I used to gather this information.

+ Rust
    + https://doc.rust-lang.org/book/
    + https://doc.rust-lang.org/rust-by-example/
    + https://docs.rs/
    + https://crates.io
    + https://serde.rs/
    + https://thefullsnack.com/en/recursive-rust.html

+ Wasm
    + https://rustwasm.github.io/
    + https://rustwasm.github.io/docs/book
    + https://github.com/WebAssembly/meetings/blob/master/process/phases.md
    + Community
        + https://www.w3.org/community/webassembly/
        + https://github.com/WebAssembly/proposals

+ Wasm WebIDL
    + https://github.com/WebAssembly/webidl-bindings/blob/master/proposals/webidl-bindings/Explainer.md
    + https://github.com/WebAssembly/meetings/blob/master/2019/CG-06.md#webidl-bindings-1-2-hrs
    + https://docs.google.com/presentation/d/1wtAknL-UJWDoIgSbyF5paTBSpVVj-fKU4tiHMxJbSzE/edit#slide=id.p

+ C++ Wasm
    + https://developers.google.com/web/updates/2018/08/embind
    + https://developers.google.com/web/updates/2019/01/emscripten-npm#docker
